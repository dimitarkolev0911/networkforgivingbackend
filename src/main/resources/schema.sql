CREATE DATABASE NetworkOfGiving

--Here I create a table with the main purpose to hold the information needed for an user
--The limits on the password are a little bit larger but this is in order to support future upgrade
--to spring security since the encoding make the password longer. Also I have decided to split the full name
--into two columns(variables) first and last name since it's more intuitive for the user. The column role with
--default value 'ROLE_USER' is put also for a future upgrade to spring security. But since it's working flawlessly
--I have decided not to rework it.

CREATE TABLE registered
(
	username VARCHAR(100) PRIMARY KEY,
	password VARCHAR(100) NOT NULL,
	firstName VARCHAR(30) NOT NULL,
	lastName VARCHAR(30) NOT NULL,
	age INT NOT NULL,
	gender VARCHAR(9) NOT NULL DEFAULT 'Undefined',
	location VARCHAR(100) NOT NULL DEFAULT 'N/A',
	enabled INT NOT NULL DEFAULT 1,
	role VARCHAR(10) NOT NULL DEFAULT 'ROLE_USER'
);

--Here I create table to hold the main information needed for a charity. It has id which generates automatically and
--increases by one on every added row. It serves as a primary key since we can assume that there can be more than one
--charity with the same name. The description is an empty string by default. And I assume that there wouldn't be a
--charity which will require more than 13 digits number since this is more than 1 trillion. Also it has the username of
--the creator so that we should know only who can edit/delete the charity. There is a column for date of creation so that
--we can sort them by first showing the newest ones and we have an url of the thumbnail and we have one default
--neutral image for donation which I find cool.

CREATE TABLE charity
(
	id INT IDENTITY(1,1) PRIMARY KEY,
	title VARCHAR(80),
	description VARCHAR(300) DEFAULT ' ',
	volunteersNeeded INT DEFAULT 0,
	volunteers INT DEFAULT 0,
	moneyNeeded DECIMAL(15,2) DEFAULT 0,
	moneyDonated DECIMAL(15,2) DEFAULT 0,
	creatorsUsername VARCHAR(30) NOT NULL,
	dateOfCreation DATETIME NOT NULL DEFAULT GETDATE(),
	imageUrl VARCHAR(300) NOT NULL DEFAULT 'https://static.vecteezy.com/system/resources/previews/000/411/613/original/vector-people-volunteering-and-donating-money-and-items-to-a-charitable-cause.jpg'
);

--The participate in table is used mainly to hold information which user to which charities has participated.
--It has a constraint that helps to restrain one user to participating more than one time in a charity.
--Basically it says that user with username X participated in charity with charityId Y on date and the default date is
--the date of the participation.

CREATE TABLE participateIn(
	username VARCHAR(30),
	charityId INT,
	date DATETIME NOT NULL DEFAULT GETDATE()
	PRIMARY KEY(username,charityId)
);

--The donateTo table has similar meaning to the participateIn table but it has one extra column donationId.
--This column(field) is used for identifying a particular donation when we want to remove it or delete it,
--since one user can donate more than one time the same amount of money to the same charity.
--And basically the meaning of the table is: User with username X made donation of amount Y to charity Z on
--particular day and this donation has Id I.

CREATE TABLE donateTo(
	donationId INT IDENTITY(1,1) PRIMARY KEY,
	username VARCHAR(30),
	charityId INT,
	donation DECIMAL(15,2),
	time DATETIME NOT NULL DEFAULT GETDATE()
);