package com.vmware.NetworkOfGiving.controller;

import com.vmware.NetworkOfGiving.model.Charity;
import com.vmware.NetworkOfGiving.service.CharityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/charity")
public class CharityController {

    private final String LOCAL_HOST_URL = "http://localhost:4200";
    private final CharityService charityService;

    @Autowired
    public CharityController(CharityService charityService) {
        this.charityService = charityService;
    }

    @CrossOrigin(origins = LOCAL_HOST_URL)
    @PutMapping(path = "/create")
    public void createCharity(@RequestBody Charity charity)
    {
        System.out.println("Creating charity");
        charityService.createCharity(charity);
    }

    @CrossOrigin(origins = LOCAL_HOST_URL)
    @DeleteMapping(path = "/delete/{id}")
    public void deleteCharity(@PathVariable("id") int id)
    {
        charityService.deleteCharity(id);
    }

    @CrossOrigin(origins = LOCAL_HOST_URL)
    @GetMapping(path = "/all")
    public List<Charity> getAllCharities()
    {
        System.out.println("charity/all");
        return charityService.getAllCharities();
    }

    @CrossOrigin(origins = LOCAL_HOST_URL)
    @GetMapping(path = "/search/{key}")
    public List<Charity> searchCharity(@PathVariable("key") String key)
    {
        return charityService.searchCharity(key);
    }

    @CrossOrigin(origins = LOCAL_HOST_URL)
    @GetMapping(path = "/get/{id}")
    public Charity getCharityById(@PathVariable("id") int id){
        try{
            System.out.println("Searching for: " + id);
            return charityService.getCharityById(id);
        } catch (Exception e) {
            return null;
        }
    }

    @CrossOrigin(origins = LOCAL_HOST_URL)
    @GetMapping(path = "/get/createdBy/{username}")
    public List<Charity> getAllCharitiesCreatedByUser(@PathVariable("username") String username){
        return charityService.getAllCharitiesCreatedByUser(username);
    }

    @CrossOrigin(origins = LOCAL_HOST_URL)
    @PutMapping("edit/{id}")
    public void editCharityById(@PathVariable("id") int id, @RequestBody Charity charity){
            charityService.editCharityById(id, charity);
    }

    @CrossOrigin(origins = LOCAL_HOST_URL)
    @GetMapping("get/donatedTo/{username}")
    public List<Charity> getAllCharityInWhichUserHasDonated(@PathVariable("username") String username){
        return charityService.getAllCharityInWhichUserHasDonated(username);
    }

    @CrossOrigin(origins = LOCAL_HOST_URL)
    @GetMapping("get/participatedIn/{username}")
    public List<Charity> getAllCharityInWhichUserHasParticipatedIn(@PathVariable("username") String username){
        return charityService.getAllCharityInWhichUserHasParticipated(username);
    }
}
