package com.vmware.NetworkOfGiving.dao;

import com.vmware.NetworkOfGiving.model.Participation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class MssqlParticipateDao implements ParticipateDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public MssqlParticipateDao(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * First it checks if there is need for more volunteers for this charity
     * and if there is none it returns and don't add new participant to this charity.
     * However if there is need of more participants it increases their amount with 1 in the charity
     * table and record it in the participateIn table.
     * @param username
     * @param charityId
     */

    @Override
    public void participateIn(String username, int charityId) {
        int maxParticipants = jdbcTemplate.queryForObject("SELECT volunteersNeeded FROM charity WHERE id LIKE ?",Integer.class,charityId);
        int participant = jdbcTemplate.queryForObject("SELECT volunteers FROM charity WHERE id LIKE ?",Integer.class,charityId);
        if(participant >= maxParticipants)
        {
            System.out.println("No more participants needed!");
            return;
        }
        jdbcTemplate.update("INSERT INTO participateIn (username, charityId) VALUES (?,?)",username,charityId);
        jdbcTemplate.update("UPDATE charity SET volunteers = volunteers + 1 WHERE id LIKE ?",charityId);
    }

    /**
     * Query that returns all of the participants in particular charity selected by id
     * and ordered by participation date starting from the last one who volunteered
     * @param id
     * @return
     */

    @Override
    public List<Participation> getAllParticipationByCharityId(int id) {
        return jdbcTemplate.query("SELECT * FROM participateIn WHERE charityId LIKE ? ORDER BY date DESC",new ParticipationMapper(), id);
    }

    /**
     * Query to remove participation of particular charity selected by it's id
     * @param username
     * @param charityId
     */

    @Override
    public void removeParticipation(String username, int charityId) {
        jdbcTemplate.update("DELETE FROM participateIn WHERE charityId = ? AND username = ?",charityId, username);
    }

    /**
     * Delete charity by username only
     * @param username
     */

    @Override
    public void removeParticipationByUsernameOnly(String username) {
        jdbcTemplate.update("DELETE FROM participateIn WHERE username = ?",username);
    }

    /**
     * Custom Participation mapper that implements RowMapper and maps the returned table
     * form the query to instances of the Participation class
     */

    private static final class ParticipationMapper implements RowMapper<Participation> {

        @Override
        public Participation mapRow(ResultSet resultSet, int i) throws SQLException {
            String username = resultSet.getString("username");
            int charityId = resultSet.getInt("charityId");
            Participation newParticipation = new Participation(username, charityId);
            return newParticipation;
        }
    }

}
