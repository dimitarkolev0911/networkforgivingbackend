package com.vmware.NetworkOfGiving.dao;

import com.vmware.NetworkOfGiving.model.Registered;

import java.util.List;
import java.util.Optional;

public interface RegisteredDao {

    List<Registered> getAllRegistered();

    Registered getRegisteredByUsername(String username);

    void registerUser(Registered registered);

    Double getAverageDonationByUsername(String username);


}
