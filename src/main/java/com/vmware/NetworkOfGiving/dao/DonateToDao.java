package com.vmware.NetworkOfGiving.dao;

import com.vmware.NetworkOfGiving.model.Donation;

import java.util.List;

public interface DonateToDao {

    void donateTo(String username, int charityId, double donation);

    List<Donation> getAllDonationsByCharityId(int id);

    void removeDonationByDonationId(int donationId);

    void editDonationByDonationId(int donateTo, double newDonation);
}
