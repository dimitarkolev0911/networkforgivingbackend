package com.vmware.NetworkOfGiving.dao;

import com.vmware.NetworkOfGiving.model.Charity;

import java.util.List;

public interface CharityDao {

    void createCharity(Charity charity);

    //void deleteCharity(Charity charity);

    void deleteCharity(int id);

    Charity getCharityById(int id);

    List<Charity> getAllCharities();

    List<Charity> searchCharity(String key);

    void editCharityById(int id, Charity charity);

    List<Charity> getAllCharitiesCreatedByUser(String username);

    List<Charity> getAllCharityInWhichUserHasDonated(String username);

    List<Charity> getAllCharityInWhichUserHasParticipated(String username);

}
