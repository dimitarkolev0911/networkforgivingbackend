package com.vmware.NetworkOfGiving.dao;

import com.vmware.NetworkOfGiving.model.Participation;

import java.util.List;

public interface ParticipateDao {

    void participateIn(String username, int charityId);

    List<Participation> getAllParticipationByCharityId(int id);

    void removeParticipation(String username, int charityId);

    void removeParticipationByUsernameOnly(String username);

}
