package com.vmware.NetworkOfGiving.dao;

import com.vmware.NetworkOfGiving.model.Charity;
import com.vmware.NetworkOfGiving.model.Donation;
import com.vmware.NetworkOfGiving.model.Participation;
import com.vmware.NetworkOfGiving.service.DonateService;
import com.vmware.NetworkOfGiving.service.ParticipateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class MssqlCharityDao implements CharityDao {

    private final JdbcTemplate jdbcTemplate;
    private final ParticipateService participateService;
    private final DonateService donateService;

    /**
     * Autowired constructor with parameter
     * the data source of the application and
     * participation service
     * @param dataSource
     * @param participateService
     * @param donateService
     */

    @Autowired
    public MssqlCharityDao(DataSource dataSource, ParticipateService participateService, DonateService donateService) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.participateService = participateService;
        this.donateService = donateService;
    }


    /**
     * Inserting a new charity into the database table charity
     * by giving parameters:
     * - the title of the charity
     * - the description of the charity
     * - the volunteers needed for the charity
     * - the volunteers that it already has (0 by default)
     * - the money needed for this charity
     * - the money which are already donated to this charity(0 by default)
     * - the username of the creator of the charity
     * @param charity
     */

    @Override
    public void createCharity(Charity charity) {
        String defaultImageUrl = "https://static.vecteezy.com/system/resources/previews/000/411/613/original/vector-people-volunteering-and-donating-money-and-items-to-a-charitable-cause.jpg";
        if(charity.getImageUrl().trim().length() == 0) charity.setImageUrl(defaultImageUrl);
        System.out.println(charity.getImageUrl());
        jdbcTemplate.update("INSERT INTO charity " +
                        "(title, description, volunteersNeeded,volunteers,moneyNeeded,moneyDonated,creatorsUsername,imageUrl) " +
                        "VALUES " +
                        "(?,?,?,?,?,?,?,?)",
                charity.getTitle(),
                charity.getDescription(),
                charity.getVolunteersNeeded(),
                charity.getVolunteers(),
                charity.getMoneyNeeded(),
                charity.getMoneyDonated(),
                charity.getCreatorsUsername(),
                charity.getImageUrl());
    }

    /**
     * Deleting a charity and by deleting the charity we delete all
     * donations made to it and all records for participating
     * @param id
     */

    @Override
    public void deleteCharity(int id) {
        jdbcTemplate.update("DELETE FROM charity WHERE id LIKE ?",id);
        jdbcTemplate.update("DELETE FROM participateIn WHERE charityId LIKE ?",id);
        jdbcTemplate.update("DELETE FROM donateTo WHERE charityId LIKE ?",id);
    }

    /**
     * Query for getting a charity by id
     * @param id
     * @returns null if there is no charity with this id
     * @returns the charity with this id if it exists
     */

    @Override
    public Charity getCharityById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM charity WHERE id LIKE ?",new CharityMapper(), id);
    }

    /**
     * Query to get all the charities ordered by id.
     * The logic behind the order is to show the newest charities
     * on the top
     * @returns all charities
     */

    @Override
    public List<Charity> getAllCharities() {
        return jdbcTemplate.query("SELECT * FROM charity ORDER BY id DESC", new CharityMapper());
    }

    /**
     * Query to find all the charities that includes the
     * key phrase in it's title without being case sensitive
     * @param key
     * @return empty list if there are no charities whose title matches the pattern
     * @return all charities whose title matches the pattern (includes the key word)
     */

    @Override
    public List<Charity> searchCharity(String key) {
        return jdbcTemplate.query("SELECT * FROM charity WHERE title LIKE ?",new CharityMapper(),"%"+key+"%");
    }

    /**
     * In this method first we see if there is charity with this id
     * (let's say we validate the input) and if there is no charity with
     * this id return and escape the method.
     * If there is charity with this id the next condition will be to check
     * if we want to decrease the amount of volunteers needed below the ones
     * that we already have. If so we will remove the difference-amount of
     * volunteers in order to fit in the new limits.
     * Then we do the same with the donations. First check if we need to edit
     * something. Then call the helper function removeUnnecessaryDonations().
     * And after we have validate this we just change the parameters with
     * a simple update query.
     * @param id
     * @param charity
     */

    @Override
    public void editCharityById(int id, Charity charity) {
        Charity oldCharity = getCharityById(id);
        if(oldCharity == null){
            return;
        }
        if(oldCharity.getVolunteers() > charity.getVolunteersNeeded()){
            int difference = oldCharity.getVolunteers() - charity.getVolunteersNeeded();
            removeUnnecessaryParticipants(difference,charity.getId());
            charity.setVolunteers(charity.getVolunteersNeeded());
        }
        if(oldCharity.getMoneyDonated() > charity.getMoneyNeeded()){
            System.out.println("Old money: " + oldCharity.getMoneyDonated());
            System.out.println("Money needed: " + charity.getMoneyNeeded());
           double difference = oldCharity.getMoneyDonated() - charity.getMoneyNeeded();
           removeUnnecessaryDonations(difference,charity.getId());
           charity.setMoneyDonated(charity.getMoneyNeeded());
        }

        jdbcTemplate.update("UPDATE charity SET title = ?, description=?, volunteersNeeded =?, " +
                                 "volunteers=?, moneyNeeded=?, moneyDonated=?, creatorsUsername=?, " +
                                 "imageUrl=? WHERE id LIKE ?",
                                  charity.getTitle(), charity.getDescription(),
                                  charity.getVolunteersNeeded(),charity.getVolunteers(),
                                  charity.getMoneyNeeded(),charity.getMoneyDonated(),
                                  charity.getCreatorsUsername(),
                                  charity.getImageUrl(),id);
    }

    /**
     * In this function I write a query to get all the charities which were created by
     * particular user, as his/her username is given as a parameter.
     * @param username
     * @return
     */

    @Override
    public List<Charity> getAllCharitiesCreatedByUser(String username) {
        return jdbcTemplate.query("SELECT  * FROM charity WHERE creatorsUsername LIKE ?",new CharityMapper(), username);
    }

    /**
     * In this function we get a list of all charities to
     * which user has donated again by passing the username as
     * an argument.
     * @param username
     * @return
     */

    @Override
    public List<Charity> getAllCharityInWhichUserHasDonated(String username) {
        return jdbcTemplate.query("SELECT * FROM charity WHERE id IN (SELECT charityId FROM donateTo WHERE username LIKE ?)",new CharityMapper(),username);

    }


    /**
     * In this function we get a list of all charities in
     * which particular user has participated again by passing
     * the username as an argument.
     * @param username
     * @return
     */

    @Override
    public List<Charity> getAllCharityInWhichUserHasParticipated(String username) {
        return jdbcTemplate.query("SELECT * FROM charity WHERE id IN (SELECT charityId FROM participateIn WHERE username LIKE ?)",new CharityMapper(),username);
    }

    /**
     * Custom mapper for the class Charity that implements the row mapper
     */

    private static final class CharityMapper implements RowMapper<Charity>
    {

        @Override
        public Charity mapRow(ResultSet resultSet, int i) throws SQLException {
            Charity charity = new Charity();
            charity.setId(resultSet.getInt("id"));
            charity.setTitle(resultSet.getString("title"));
            charity.setDescription(resultSet.getString("description"));
            charity.setVolunteersNeeded(resultSet.getInt("volunteersNeeded"));
            charity.setVolunteers(resultSet.getInt("volunteers"));
            charity.setMoneyNeeded(resultSet.getDouble("moneyNeeded"));
            charity.setMoneyDonated(resultSet.getDouble("moneyDonated"));
            charity.setCreatorsUsername(resultSet.getString("creatorsUsername"));
            charity.setImageUrl(resultSet.getString("imageUrl"));
            return charity;
        }
    }

    /**
     * Helping function to remove all unnecessary participants from a charity when we edit it
     * @param difference
     * @param charityId
     */

    private void removeUnnecessaryParticipants(int difference, int charityId){
        List<Participation> participations = participateService.getAllParticipationByCharityId(charityId);
        for(int i=0;i<difference;i++){
            Participation participation = participations.get(i);
            participateService.removeParticipation(participation.getUsername(),
                    participation.getCharityId());
        }
    }

    /**
     * Helping function to remove all unnecessary donations from a charity when we edit it
     * @param difference
     * @param charityId
     */

    private void removeUnnecessaryDonations(double difference, int charityId){
        List<Donation> donations = donateService.getAllDonationsByCharityId(charityId);
        int i=0;
        while(true){
            Donation currentDonation = donations.get(i);
            System.out.println("Difference is: " + difference);
            System.out.printf("Current donation is: \nDonationId: %d \nCharityId: %d \nDonation: %f",currentDonation.getDonationId(), currentDonation.getCharityId(), currentDonation.getDonation());
            i++;
            if(difference - currentDonation.getDonation() <= 0){
                double newDonation = currentDonation.getDonation() - difference;
                donateService.editDonationByDonationId(currentDonation.getDonationId(),newDonation);
                System.out.println("Break");
                break;
            }
            else{
                donateService.removeDonationByDonationId(currentDonation.getDonationId());
                difference -= currentDonation.getDonation();
            }
        }
    }
}
