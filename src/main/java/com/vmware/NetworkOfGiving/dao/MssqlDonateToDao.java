package com.vmware.NetworkOfGiving.dao;

import com.vmware.NetworkOfGiving.model.Donation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class MssqlDonateToDao implements DonateToDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public MssqlDonateToDao(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * Query to make a donation to a charity
     * if the amount entered to donate is larger than what is needed
     * make the donation smaller in order to fit in the needed money
     * e.g. if the needed money is 30 000$ and 29 000$ are already
     * and the user tries to donate 2 000$ only 1 000$ will be donated
     * @param username
     * @param charityId
     * @param donation
     */

    @Override
    public void donateTo(String username, int charityId, double donation) {
        double maxDonation = jdbcTemplate.queryForObject("SELECT moneyNeeded FROM charity WHERE id LIKE ?",
                                                               Double.class,charityId);
        double amountOfDonations = jdbcTemplate.queryForObject("SELECT moneyDonated FROM charity WHERE id LIKE ?",
                                                                     Double.class,charityId);
        double currentDonation = (maxDonation-amountOfDonations) < donation?(maxDonation-amountOfDonations):donation;
        if(currentDonation == 0)
        {
            System.out.println("No more donations needed!");
            return;
        }
        jdbcTemplate.update("INSERT INTO donateTo (username, charityId, donation) VALUES (?,?,?)",
                                  username,charityId,currentDonation);
        jdbcTemplate.update("UPDATE charity SET moneyDonated = moneyDonated + ? WHERE id LIKE ?",
                                  currentDonation,charityId);
    }

    /**
     * Get all the donations to charity with this id
     * @param id
     * @return list of donations
     */

    @Override
    public List<Donation> getAllDonationsByCharityId(int id) {
        return jdbcTemplate.query("SELECT * FROM donateTo WHERE charityId LIKE ? ORDER BY time DESC",
                                        new DonationMapper(), id);
    }

    /**
     * Deleting a donation from the database by passing the donation id
     * @param donationId
     */


    @Override
    public void removeDonationByDonationId(int donationId) {
        jdbcTemplate.update("DELETE  FROM donateTo WHERE donationId LIKE ?",donationId);
    }

    /**
     * Editing a donation by setting new amount to be donated using the donation id.
     * @param donationId
     * @param newDonation
     */

    @Override
    public void editDonationByDonationId(int donationId, double newDonation) {
        jdbcTemplate.update("UPDATE donateTo SET donation = ? WHERE donationId LIKE ?",newDonation, donationId);
    }

    /**
     * Custom mapper for the class Donation that implements RowMapper interface
     */

    private static final class DonationMapper implements RowMapper<Donation>{

        @Override
        public Donation mapRow(ResultSet resultSet, int i) throws SQLException {
            String username = resultSet.getString("username");
            int charityId = resultSet.getInt("charityId");
            double donation = resultSet.getDouble("donation");
            int donationId = resultSet.getInt("donationId");
            Donation newDonation = new Donation(username, charityId, donation, donationId);
            return newDonation;
        }
    }


}
