package com.vmware.NetworkOfGiving.model;

import java.util.Objects;

public class Donation {
    private final String username;
    private final int charityId;
    private final double donation;
    private final int donationId;

    /**
     * Parametrized constructor
     * @param username
     * @param charityId
     * @param donation
     * @param donationId
     */

    public Donation(String username, int charityId, double donation, int donationId) {
        this.username = username;
        this.charityId = charityId;
        this.donation = donation;
        this.donationId = donationId;
    }

    /**
     * Getter for the username of the user who is donating
     * @return username
     */

    public String getUsername() {
        return username;
    }

    /**
     * Getter for the id of the charity to which the user is donating
     * @return charityId
     */

    public int getCharityId() {
        return charityId;
    }

    /**
     * Getter for the amount of the donation
     * @return donation
     */

    public double getDonation() {
        return donation;
    }

    public int getDonationId() {
        return donationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Donation donation1 = (Donation) o;
        return charityId == donation1.charityId &&
                Double.compare(donation1.donation, donation) == 0 &&
                donationId == donation1.donationId &&
                username.equals(donation1.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, charityId, donation, donationId);
    }
}
