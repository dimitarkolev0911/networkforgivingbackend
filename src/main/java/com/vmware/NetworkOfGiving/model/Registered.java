package com.vmware.NetworkOfGiving.model;

public class Registered {

    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private int age;
    private Gender gender;
    private String location;

    /**
     * Default constructor
     */

    public Registered()
    {

    }

    /**
     * Parametrized constructor for a Registered object
     * @param username
     * @param password
     * @param firstName
     * @param lastName
     * @param age
     * @param gender
     * @param location
     */

    public Registered(String username, String password, String firstName, String lastName, int age, Gender gender, String location) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
        this.location = location;
    }

    /**
     * Getter for the username of the registered user
     * @return username
     */

    public String getUsername() {
        return username;
    }

    /**
     * Setter for the username
     * @param username
     */

    public void setUsername(String username) {
        this.username = username;
    }


    /**
     * Getter for the password
     * @return password
     */

    public String getPassword() {
        return password;
    }

    /**
     * Setter for the password
     * @param password
     */

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter for the first name
     * @return firstname
     */

    public String getFirstName() {
        return firstName;
    }


    /**
     * Setter for the first name
     * @param firstName
     */

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter for the last name
     * @return lastName
     */

    public String getLastName() {
        return lastName;
    }

    /**
     * Setter for the last name
     * @param lastName
     */

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter for the age
     * @return age
     */

    public int getAge() {
        return age;
    }


    /**
     * Setter for the age
     * @param age
     */

    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Getter for the gender
     * @return gender
     */

    public Gender getGender() {
        return gender;
    }

    /**
     * Setter for the gender
     * @param gender
     */

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    /**
     * Getter for the location
     * @return location
     */

    public String getLocation() {
        return location;
    }

    /**
     * Setter for the location
     * @param location
     */

    public void setLocation(String location) {
        this.location = location;
    }
}
