package com.vmware.NetworkOfGiving.model;

import java.util.Iterator;
import java.util.Objects;

public class Charity {

    private int id;

    private String title;
    private String description;
    private int volunteersNeeded;
    private int volunteers;
    private double moneyNeeded;
    private double moneyDonated;
    private String creatorsUsername;
    private String imageUrl;

    public Charity() {

    }

    /**
     *
     * @param title
     * @param description
     * @param volunteersNeeded
     * @param volunteers
     * @param moneyNeeded
     * @param moneyDonated
     * @param creatorsUsername
     * @param id
     */

    public Charity(String title, String description, int volunteersNeeded, int volunteers, double moneyNeeded,
                   double moneyDonated, String creatorsUsername, int id, String imageUrl) {
        this.title = title;
        this.description = description;
        this.volunteersNeeded = volunteersNeeded;
        this.volunteers = volunteers;
        this.moneyNeeded = moneyNeeded;
        this.moneyDonated = moneyDonated;
        this.creatorsUsername = creatorsUsername;
        this.id = id;
        this.imageUrl = imageUrl;
    }

    /**
     * @returns title
     */

    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     */

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return description
     */

    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return volunteersNeeded
     */

    public int getVolunteersNeeded() {
        return volunteersNeeded;
    }

    /**
     *
     * @param volunteersNeeded
     */

    public void setVolunteersNeeded(int volunteersNeeded) {
        this.volunteersNeeded = volunteersNeeded;
    }

    /**
     *
     * @return volunteers
     */

    public int getVolunteers() {
        return volunteers;
    }

    /**
     *
     * @param volunteers
     */

    public void setVolunteers(int volunteers) {
        this.volunteers = volunteers;
    }

    /**
     *
     * @return moneyNeeded
     */

    public double getMoneyNeeded() {
        return moneyNeeded;
    }

    /**
     *
     * @param moneyNeeded
     */

    public void setMoneyNeeded(double moneyNeeded) {
        this.moneyNeeded = moneyNeeded;
    }

    /**
     *
     * @return moneyDonated
     */

    public double getMoneyDonated() {
        return moneyDonated;
    }

    /**
     *
     * @param moneyDonated
     */

    public void setMoneyDonated(double moneyDonated) {
        this.moneyDonated = moneyDonated;
    }

    /**
     *
     * @return creatorsUsername
     */

    public String getCreatorsUsername() {
        return creatorsUsername;
    }

    /**
     *
     * @param creatorsUsername
     */

    public void setCreatorsUsername(String creatorsUsername) {
        this.creatorsUsername = creatorsUsername;
    }

    /**
     *
     * @return id
     */

    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Charity charity = (Charity) o;
        return id == charity.id &&
                volunteersNeeded == charity.volunteersNeeded &&
                volunteers == charity.volunteers &&
                Double.compare(charity.moneyNeeded, moneyNeeded) == 0 &&
                Double.compare(charity.moneyDonated, moneyDonated) == 0 &&
                title.equals(charity.title) &&
                description.equals(charity.description) &&
                creatorsUsername.equals(charity.creatorsUsername) &&
                Objects.equals(imageUrl, charity.imageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, volunteersNeeded, volunteers, moneyNeeded, moneyDonated, creatorsUsername, imageUrl);
    }

}
