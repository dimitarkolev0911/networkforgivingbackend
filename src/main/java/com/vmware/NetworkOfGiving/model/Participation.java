package com.vmware.NetworkOfGiving.model;

public class Participation {
    private final String username;
    private final int charityId;

    /**
     * Parametrized constructor for Participation
     * @param username
     * @param charityId
     */

    public Participation(String username, int charityId) {
        this.username = username;
        this.charityId = charityId;
    }

    /**
     * Getter for the username
     * @return username
     */

    public String getUsername() {
        return username;
    }

    /**
     * Getter for the id of the charity in which the user
     * is participating
     * @return charityId
     */

    public int getCharityId() {
        return charityId;
    }
}
