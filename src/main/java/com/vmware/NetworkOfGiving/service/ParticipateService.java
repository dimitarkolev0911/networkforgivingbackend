package com.vmware.NetworkOfGiving.service;

import com.vmware.NetworkOfGiving.dao.MssqlParticipateDao;
import com.vmware.NetworkOfGiving.model.Participation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParticipateService {

    private final MssqlParticipateDao mssqlParticipateDao;

    @Autowired
    public ParticipateService(MssqlParticipateDao mssqlParticipateDao) {
        this.mssqlParticipateDao = mssqlParticipateDao;
    }

    public void participateIn(String username, int charityId)
    {
        mssqlParticipateDao.participateIn(username, charityId);
    }

    public List<Participation> getAllParticipationByCharityId(int id) {
        return mssqlParticipateDao.getAllParticipationByCharityId(id);
    }

    public void removeParticipationByUsernameOnly(String username) {
        mssqlParticipateDao.removeParticipationByUsernameOnly(username);
    }

    public void removeParticipation(String username, int id){
        mssqlParticipateDao.removeParticipation(username, id);
    }
}
