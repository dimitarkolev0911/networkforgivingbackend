package com.vmware.NetworkOfGiving.service;

import com.vmware.NetworkOfGiving.dao.MssqlDonateToDao;
import com.vmware.NetworkOfGiving.model.Donation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DonateService {

    private final MssqlDonateToDao mssqlDonateToDao;

    @Autowired
    public DonateService(MssqlDonateToDao mssqlDonateToDao) {
        this.mssqlDonateToDao = mssqlDonateToDao;
    }

    public void donateTo(String username, int charityId, double donation)
    {
        mssqlDonateToDao.donateTo(username, charityId, donation);
    }

    public List<Donation> getAllDonationsByCharityId(int id) {
        return mssqlDonateToDao.getAllDonationsByCharityId(id);
    }

    public void removeDonationByDonationId(int donationId) {
        mssqlDonateToDao.removeDonationByDonationId(donationId);
    }

    public void editDonationByDonationId(int donationId, double newDonation) {
        mssqlDonateToDao.editDonationByDonationId(donationId, newDonation);
    }
}
