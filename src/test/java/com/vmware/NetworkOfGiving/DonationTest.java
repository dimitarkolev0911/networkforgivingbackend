package com.vmware.NetworkOfGiving;

import com.vmware.NetworkOfGiving.dao.MssqlDonateToDao;
import com.vmware.NetworkOfGiving.dao.MssqlParticipateDao;
import com.vmware.NetworkOfGiving.dao.MssqlRegisteredDao;
import com.vmware.NetworkOfGiving.model.Donation;
import com.vmware.NetworkOfGiving.service.DonateService;
import com.vmware.NetworkOfGiving.service.ParticipateService;
import com.vmware.NetworkOfGiving.service.RegisteredService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class DonationTest {
    @TestConfiguration
    static class Setter {
        @Bean
        @Autowired
        public DonateService donateService(MssqlDonateToDao mssqlDonateToDao){
            return new DonateService(mssqlDonateToDao);
        }

    }

    @Autowired
    private DonateService donateService;

    @MockBean
    private MssqlDonateToDao mssqlDonateToDao;

    @Before
    public void setUp(){
        Donation donation = new Donation(
                "dimitar",
                1,
                100.50,
                1
        );

        Mockito.when(donateService.getAllDonationsByCharityId(1))
                .thenReturn(Arrays.asList(donation));
    }

    @Test
    public void IfThereAreDonationsToThisId_ShouldReturnListOfThem(){
        //Before
        int charityId = 1;

        //Given
        List<Donation> donations = donateService.getAllDonationsByCharityId(charityId);
        Donation donation = new Donation("dimitar", 1, 100.50, 1);

        //Then
        assertThat(donations,hasItems(donation));
    }

    @Test
    public void IfThereAreNoDonationsToThisId_ShouldReturnEmptyList(){
        //Before
        int charityId = 2;

        //Given
        List<Donation> donations = donateService.getAllDonationsByCharityId(charityId);

        //Then
        Assertions.assertThat(donations)
                .isEqualTo(Collections.emptyList());
    }

    @Test
    public void IfEditOnDonationIsSuccessful(){
        //Before
        Donation donation = new Donation("dimitar",1,100,1);
        double newDonation = 150;

        //Given
        donateService.editDonationByDonationId(1,newDonation);

        //Then
        Assertions.assertThat(donation.getDonation())
                .isEqualTo(newDonation);
    }

}
