package com.vmware.NetworkOfGiving;

import com.vmware.NetworkOfGiving.dao.MssqlParticipateDao;
import com.vmware.NetworkOfGiving.dao.MssqlRegisteredDao;
import com.vmware.NetworkOfGiving.dao.RegisteredDao;
import com.vmware.NetworkOfGiving.model.Gender;
import com.vmware.NetworkOfGiving.model.Registered;
import com.vmware.NetworkOfGiving.service.ParticipateService;
import com.vmware.NetworkOfGiving.service.RegisteredService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class RegisteredTest {
    @TestConfiguration
    static class helper {
        @Bean
        @Autowired
        public RegisteredService registeredService(MssqlRegisteredDao registeredDao){
            return new RegisteredService(registeredDao);
        }
    }

    @Autowired
    private RegisteredService registeredService;

    @MockBean
    private MssqlRegisteredDao mssqlRegisteredDao;

    @Before
    public void setUp() {
        Registered me = new Registered(
                "dimitar",
                "123456",
                "Dimitar",
                "Kolev",
                20,
                Gender.Male,
                "Sofia"
        );

        Mockito.when(mssqlRegisteredDao.getRegisteredByUsername(me.getUsername()))
                .thenReturn(me);
    }

    @Test
    public void whenValidUsername_thenRegisteredShouldBeFound() {
        String username = "dimitar";
        Registered found = registeredService.getRegisteredByUsername(username);
        assertThat(found.getUsername())
                .isEqualTo(username);
    }

    @Test
    public void whenInvalidUsername_thenNullShouldBeReturned(){
        String username = "dimitarK";
        Registered found = registeredService.getRegisteredByUsername(username);
        System.out.println(found);
        assertThat(found).isEqualTo(null);
    }


}
