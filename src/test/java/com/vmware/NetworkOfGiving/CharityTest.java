package com.vmware.NetworkOfGiving;

import com.vmware.NetworkOfGiving.controller.CharityController;
import com.vmware.NetworkOfGiving.dao.MssqlCharityDao;
import com.vmware.NetworkOfGiving.model.Charity;
import com.vmware.NetworkOfGiving.service.CharityService;
import net.bytebuddy.description.annotation.AnnotationList;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
public class CharityTest {

    @TestConfiguration
    static class Setter {
        @Bean
        @Autowired
        public CharityService charityService(MssqlCharityDao mssqlCharityDao){
            return new CharityService(mssqlCharityDao);
        }
    }

    @Autowired
    private CharityService charityService;

    @MockBean
    private MssqlCharityDao mssqlCharityDao;

    @Before
    public void setUp(){
        Charity charity = new Charity(
                "Test charity",
                "This is test charity.",
                0,
                0,
                1000,
                0,
                "dimitar",
                1,
                "image.png"
        );

        Mockito.when(mssqlCharityDao.getCharityById(1))
                .thenReturn(charity);
        Mockito.when(mssqlCharityDao.getAllCharities())
                .thenReturn(Arrays.asList(charity));
        Mockito.when(mssqlCharityDao.searchCharity("test"))
                .thenReturn(Arrays.asList(charity));
        Mockito.when(mssqlCharityDao.getAllCharitiesCreatedByUser("dimitar"))
                .thenReturn(Arrays.asList(charity));
    }


    @Test
    public void IfIndexIsValid_ShouldReturnCharity(){
        //Before
        int id=1;
        String title="Test charity";

        //Given
        Charity currentCharity = mssqlCharityDao.getCharityById(id);

        //Then
        assertThat(currentCharity.getTitle())
                .isEqualTo(title);
    }

    @Test
    public void IfIndexIsInvalid_ShouldReturnNull(){
        //Before
        int id=2;

        //Given
        Charity found = mssqlCharityDao.getCharityById(id);

        //Then
        assertThat(found).isEqualTo(null);
    }

    @Test
    public void IfThereAreResults_ThenReturnListOfCharities(){
        //Before
        String key="test";
        Charity charity = new Charity(
                "Test charity",
                "This is test charity.",
                0,
                0,
                1000,
                0,
                "dimitar",
                1,
                "image.png"
        );

        //Given
        List<Charity> charities = mssqlCharityDao.searchCharity(key);

        //Then
        assertThat(charity)
                .isIn(charities);
    }

    @Test
    public void IfThereAreNoResult_ThenShouldReturnEmptyList(){
        //Before
        String key = "panda";

        //Given
        List<Charity> charities = mssqlCharityDao.searchCharity(key);

        //Then
        assertThat(charities)
                .isEqualTo(Collections.emptyList());
    }

    @Test
    public void IfThereAreCharitiesCreatedByUser_ThenShouldReturnListOfCharities(){
        //Before
        String username = "dimitar";
        Charity charity = new Charity("Test charity", "This is test charity.", 0, 0,
                1000, 0, "dimitar", 1, "image.png");

        //Given
        List<Charity> charities = mssqlCharityDao.getAllCharitiesCreatedByUser(username);

        //Then
        assertThat(charity).isIn(charities);
    }

    @Test
    public void IfThereAreNoCharitiesCreatedByUser_ThenShouldReturnEmptyList(){
        //Before
        String username="kolev";

        //Given
        List<Charity> charities = mssqlCharityDao.getAllCharitiesCreatedByUser(username);

        //Then
        assertThat(charities)
                .isEqualTo(Collections.emptyList());
    }

}
