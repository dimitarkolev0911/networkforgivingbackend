The **NetworkForGiving** is a simple spring boot aplication that support HTTP requests to:
1. Register as a user or login if you already have an account;
1. Create a charity;
1. Edit charity if you were the one who created it;
1. Delete charity if you are it's creator;
1. Participate to a charity;
1. Donate to a charity;